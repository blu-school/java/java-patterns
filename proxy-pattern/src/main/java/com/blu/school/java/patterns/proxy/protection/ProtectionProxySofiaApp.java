package com.blu.school.java.patterns.proxy.protection;

import com.blu.school.java.patterns.proxy.protection.impl.ReportGeneratorProtectionProxy;
import com.blu.school.java.patterns.proxy.protection.staff.Staff;
import com.blu.school.java.patterns.proxy.protection.staff.impl.Employee;
import com.blu.school.java.patterns.proxy.protection.staff.impl.Owner;
import com.blu.school.java.patterns.proxy.remote.service.ReportGenerator;
import com.blu.school.java.patterns.proxy.remote.service.sofia.ReportGeneratorSofiaApp;

/**
 * Please start the {@link ReportGeneratorSofiaApp} service in order to have a successful run of the {
 *
 * @author Petko G. Dragoev
 * @link ProtectionProxySofiaApp} class
 */
public class ProtectionProxySofiaApp {

    public static void main(String[] args) {
        final Owner owner = new Owner();
        owner.setReportGenerator(getReportGeneratorForSofia(owner));

        final Employee employee = new Employee();
        employee.setReportGenerator(getReportGeneratorForSofia(employee));

        System.out.println("=================== Owner ===================");
        System.out.println(owner.generateReport());

        System.out.println("=================== Employee ===================");
        System.out.println(employee.generateReport());
    }

    private static ReportGenerator getReportGeneratorForSofia(final Staff staff) {
        return new ReportGeneratorProtectionProxy(staff, ReportGeneratorSofiaApp.SERVICE_NAME);
    }
}
