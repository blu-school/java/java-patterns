package com.blu.school.java.patterns.proxy.remote.service.plovdiv;

import com.blu.school.java.patterns.proxy.remote.service.ReportGenerator;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Date;

/**
 * This is the report generator service for the location Plovdiv. It is the actual implementation of the
 * {@link ReportGenerator} interface which generates the daily pizza report for Plovdiv.
 *
 * @author Petko G. Dragoev
 */
public class ReportGeneratorServicePlovdiv extends UnicastRemoteObject implements ReportGenerator {

    private static final String LOCATION_ID = "BG_4000_11";

    public ReportGeneratorServicePlovdiv() throws RemoteException {
        super();
    }

    @Override
    public String generateDailyReport() throws RemoteException {
        final var sb = new StringBuilder();

        sb.append("******************** Daily Pizza Report for Plovdiv ********************");
        sb.append("\r\n Location ID: ").append(LOCATION_ID);
        sb.append("\r\n Report generation date: ").append(new Date());
        sb.append("\r\n Total Pizza's sold: 50");
        sb.append("\r\n Total gross profit: 1267 lv.");
        sb.append("\r\n Total net profit: 985 lv.");
        sb.append("\r\n ***************************************************************");

        return sb.toString();
    }
}
