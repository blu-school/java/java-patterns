package com.blu.school.java.patterns.proxy.protection.staff;

import com.blu.school.java.patterns.proxy.protection.staff.impl.Employee;
import com.blu.school.java.patterns.proxy.protection.staff.impl.Owner;
import com.blu.school.java.patterns.proxy.remote.service.ReportGenerator;

/**
 * This is the protection proxy which will check if the object trying to generate the report is allowed to do so.
 * It will be implemented by the {@link Owner}
 * and {@link Employee} classes.
 *
 * @author Petko G. Dragoev
 */
public interface Staff {

    boolean isOwner();

    void setReportGenerator(ReportGenerator reportGenerator);

    String generateReport();
}

