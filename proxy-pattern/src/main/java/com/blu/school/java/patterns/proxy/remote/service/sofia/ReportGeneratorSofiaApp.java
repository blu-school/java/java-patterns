package com.blu.school.java.patterns.proxy.remote.service.sofia;

import com.blu.school.java.patterns.proxy.remote.client.ReportGeneratorClient;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 * This is a simple main class used to start the {@link ReportGeneratorServiceSofia} service for Sofia.
 *
 * @author Petko G. Dragoev
 */
public class ReportGeneratorSofiaApp {

    public final static String SERVICE_NAME = "PizzaCoRemoteGeneratorServiceSofia";

    public static void main(String[] args) {
        try {
            final Registry registry = LocateRegistry.createRegistry(ReportGeneratorClient.RMI_PORT);
            registry.rebind(SERVICE_NAME, new ReportGeneratorServiceSofia());
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }
}
