package com.blu.school.java.patterns.proxy.remote.client;

import com.blu.school.java.patterns.proxy.remote.service.ReportGenerator;
import com.blu.school.java.patterns.proxy.remote.service.plovdiv.ReportGeneratorPlovdivApp;
import com.blu.school.java.patterns.proxy.remote.service.sofia.ReportGeneratorSofiaApp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

public class ReportGeneratorClient {

    public static final String RMI_HOST = "rmi://127.0.0.1";
    public static final int RMI_PORT = 1099;

    public static void main(String[] args) {
        final var reportGeneratorClient = new ReportGeneratorClient();

        System.out.println("======= Pizza report generation system =======");
        System.out.println("Up and running ... ");
        System.out.println("Please choose on of the following options: ");
        reportGeneratorClient.printOptions();

        boolean runApp = true;

        while (runApp) {
            try {
                final var reader = new BufferedReader(new InputStreamReader(System.in));
                final var input = reader.readLine();
                switch (input.toLowerCase()) {
                    case "s":
                        reportGeneratorClient.generateReportForSofia();
                        break;
                    case "p":
                        reportGeneratorClient.generateReportForPlovdiv();
                        break;
                    case "o":
                        reportGeneratorClient.printOptions();
                        break;
                    case "x":
                        runApp = false;
                        break;
                    default:
                        System.out.println("Invalid option selection. Please try again or if you are not sure view the allowed options");
                }
            } catch (IOException e) {
                runApp = false;
                System.err.println(e.getMessage());
            }
        }

        System.out.println("System shutdown");
    }

    private void generateReportForSofia() {
        try {
            final var sofiaReportGenerator = this.getReportGenerator(ReportGeneratorSofiaApp.SERVICE_NAME);
            System.out.println(sofiaReportGenerator.generateDailyReport());
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    private void generateReportForPlovdiv() {
        try {
            final var sofiaReportGenerator = this.getReportGenerator(ReportGeneratorPlovdivApp.SERVICE_NAME);
            System.out.println(sofiaReportGenerator.generateDailyReport());
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    private ReportGenerator getReportGenerator(final String serviceName) throws MalformedURLException, NotBoundException, RemoteException {
        final var sb = new StringBuilder(RMI_HOST).append(":").append(RMI_PORT).append("/").append(serviceName);
        final var conString = sb.toString();
        return (ReportGenerator) Naming.lookup(conString);
    }

    private void printOptions() {
        System.out.println("===============================================");
        System.out.println("[s] - Generate report for Sofia");
        System.out.println("[p] - Generate report for Plovdiv");
        System.out.println("[o] - Print options");
        System.out.println("[x] - Shutdown system");
        System.out.println("===============================================");
    }
}
