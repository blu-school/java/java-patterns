package com.blu.school.java.patterns.proxy.remote.service;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * This is the remote interface which provides the methods a client can call remotely.
 * This interface should be implemented by the stub and the actual service at each pizza shop location.
 *
 * @author Petko G. Dragoev
 */
public interface ReportGenerator extends Remote {

    String generateDailyReport() throws RemoteException;
}
