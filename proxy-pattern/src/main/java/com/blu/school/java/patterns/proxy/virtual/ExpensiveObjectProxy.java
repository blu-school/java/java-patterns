package com.blu.school.java.patterns.proxy.virtual;

import com.blu.school.java.patterns.proxy.virtual.impl.ExpensiveObjectImpl;

/**
 * This is the proxy class which will prevent the initialisation of the actual heavy object @{@link ExpensiveObjectImpl}
 * being created until the process method is actually called for the first time.
 *
 * @author Petko G. Dragoev
 */
public class ExpensiveObjectProxy implements ExpensiveObject {

    private static ExpensiveObject expensiveObject;

    @Override
    public void process() {
        if (expensiveObject == null) {
            expensiveObject = new ExpensiveObjectImpl();
        }

        expensiveObject.process();
    }
}
