package com.blu.school.java.patterns.proxy.virtual;

/**
 * This is the proxy interface which will be implemented by both the actual object implementation class (which has
 * a heavy initial configuration {@link com.blu.school.java.patterns.proxy.virtual.impl.ExpensiveObjectImpl})
 * and the proxy object (which will generate the actual object class once the interface method is called {@link ExpensiveObjectProxy}).
 *
 * @author Petko G. Dragoev
 */
public interface ExpensiveObject {

    void process();

}
