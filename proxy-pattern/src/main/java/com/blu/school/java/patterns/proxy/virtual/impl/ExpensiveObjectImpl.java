package com.blu.school.java.patterns.proxy.virtual.impl;

import com.blu.school.java.patterns.proxy.virtual.ExpensiveObject;

/**
 * This is the actual object class which has the heavy initial configuration.
 *
 * @author Petko G. Dragoev
 */
public class ExpensiveObjectImpl implements ExpensiveObject {

    public ExpensiveObjectImpl() {
        init();
    }

    @Override
    public void process() {
        System.out.println("Processing complete!");
    }

    private void init() {
        System.out.println("Loading initial configuration for " + this.getClass().getName() + "...");
    }
}
