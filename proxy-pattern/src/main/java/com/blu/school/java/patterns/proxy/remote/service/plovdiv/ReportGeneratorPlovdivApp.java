package com.blu.school.java.patterns.proxy.remote.service.plovdiv;

import java.rmi.Naming;

/**
 * This is a simple main class used to start the {@link ReportGeneratorServicePlovdiv} service.
 * Please note that you have to first start the {@link com.blu.school.java.patterns.proxy.remote.service.sofia.ReportGeneratorServiceSofia} service
 * in order to run the remote method registry.
 *
 * @author Petko G. Dragoev
 */
public class ReportGeneratorPlovdivApp {

    public final static String SERVICE_NAME = "PizzaCoRemoteGeneratorPlovdivSofia";

    public static void main(String[] args) {
        try {
            Naming.rebind(SERVICE_NAME, new ReportGeneratorServicePlovdiv());
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }
}
