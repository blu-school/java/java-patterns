package com.blu.school.java.patterns.proxy.protection.staff.impl;

import com.blu.school.java.patterns.proxy.protection.staff.Staff;
import com.blu.school.java.patterns.proxy.remote.service.ReportGenerator;

/**
 * This is the owner implementation of the {@link Staff} interface. Owners are in general allowed to generate the reports.
 *
 * @author Petko G. Dragoev
 */
public class Owner implements Staff {

    private ReportGenerator reportGenerator;

    @Override
    public boolean isOwner() {
        return true;
    }

    @Override
    public void setReportGenerator(ReportGenerator reportGenerator) {
        this.reportGenerator = reportGenerator;
    }

    @Override
    public String generateReport() {
        String generatedReport = null;
        try {
            generatedReport = reportGenerator.generateDailyReport();
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }

        return generatedReport;
    }
}
