package com.blu.school.java.patterns.proxy.protection.impl;

import com.blu.school.java.patterns.proxy.protection.staff.Staff;
import com.blu.school.java.patterns.proxy.remote.client.ReportGeneratorClient;
import com.blu.school.java.patterns.proxy.remote.service.ReportGenerator;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

/**
 * This is the actual implementation of the protection proxy. It will check if the provided staff member is allowed to
 * generate the report. If that is the case then the report will be generated for the given service.
 *
 * @author Petko G. Dragoev
 */
public class ReportGeneratorProtectionProxy implements ReportGenerator {

    private ReportGenerator reportGenerator;
    private Staff staff;
    private String serviceName;

    public ReportGeneratorProtectionProxy(final Staff staff, final String serviceName) {
        this.staff = staff;
        this.serviceName = serviceName;
    }

    @Override
    public String generateDailyReport() throws RemoteException {
        if (!this.staff.isOwner()) {
            return "Not authorized to generate report. Only the owner can generate a report";
        }

        String generatedReport = null;
        try {
            this.reportGenerator = this.getReportGenerator(this.serviceName);
            generatedReport = this.reportGenerator.generateDailyReport();
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }

        return generatedReport;
    }

    private ReportGenerator getReportGenerator(final String serviceName) throws MalformedURLException, NotBoundException, RemoteException {
        final var sb = new StringBuilder(ReportGeneratorClient.RMI_HOST).append(":").append(ReportGeneratorClient.RMI_PORT).append("/").append(serviceName);
        final var conString = sb.toString();
        return (ReportGenerator) Naming.lookup(conString);
    }
}
