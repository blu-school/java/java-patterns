package com.blu.school.java.patterns.proxy.virtual;

public class VirtualProxyApp {

    public static void main(String[] args) {
        final var expensiveObject = new ExpensiveObjectProxy();

        System.out.println("=============== First process call ===============");
        expensiveObject.process();

        System.out.println("=============== Second process call ===============");
        expensiveObject.process();
    }
}
