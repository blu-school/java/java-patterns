package com.blu.school.java.patterns.proxy.protection.staff.impl;

import com.blu.school.java.patterns.proxy.protection.staff.Staff;
import com.blu.school.java.patterns.proxy.remote.service.ReportGenerator;

/**
 * This is the employee implementation of the {@link Staff} interface. Employees are in general not allowed to
 * generate the reports.
 *
 * @author Petko G. Dragoev
 */
public class Employee implements Staff {

    private ReportGenerator reportGenerator;

    @Override
    public boolean isOwner() {
        return false;
    }

    @Override
    public void setReportGenerator(final ReportGenerator reportGenerator) {
        this.reportGenerator = reportGenerator;
    }

    @Override
    public String generateReport() {
        String generatedReport = null;
        try {
            generatedReport = reportGenerator.generateDailyReport();
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }

        return generatedReport;
    }
}
