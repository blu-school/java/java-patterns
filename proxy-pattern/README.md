# Proxy structural pattern

## 1. Introduction

The proxy pattern is part of the structural java patterns and is used as a surrogate or placeholder for another object
to control access to it. There are various ways to put this pattern into use. The three most known and used options are
remote, virtual and protection proxy.

## 2. What is the proxy pattern?

The proxy pattern is used to create a representative object that will control access to another object. This other
object can either be remote, expensive to create upon initialisation or in need of being secured.

## 3. Remote proxy

The remote proxy is used when the actual service is someplace else than the client using that service (RMI). It provides
a local representative for an object in a different address space.

### Java example explained

There is a Pizza Company, which has its outlets at various locations. The owner of the company gets a daily report by
the staff members of the company from various outlets. The current application supported by the Pizza Company is a
desktop application, not a web application. So, the owner has to ask his employees to generate the report and send it to
him. But now the owner wants to generate and check the report by his own, so that he can generate it whenever he wants
without anyone’s help.

The problem here is that all applications are running at their respective JVMs and the Report Checker application should
run in the owner’s local system. The object required to generate the report does not exist in the owner’s system JVM and
you cannot directly call on the remote object.

The client object acts like its making remote method calls. But it is calling methods on a heap-local proxy object that
handles all the low-level details of network communication.

## 4. Virtual proxy

The virtual proxy is very straightforward. It is used to prevent the creation of expensive objects upon initialisation.
So it actually creates expensive objects on demand

### Java example explained

We have an interface with the `void process();` method. Then the actual heavy object that implements the
interface `ExpensiveObjectImpl.class` and the proxy object to it. When we first initialize the proxy object in our main
method the actual heavy object is not created. It is only created once when we actually call the process method.

## 5. Protection proxy

The protection proxy should be seen as an extension to the remote proxy. It is an extra layer which simply checks if the
call is authorised and may go through.

## 6. When to use the proxy pattern?

Proxy is applicable whenever there is a need for a more versatile or sophisticated reference to an object than a simple
pointer. Here are several common situations in which the Proxy pattern is applicable:

1. A remote proxy provides a local representative for an object in a different address space.
2. A virtual proxy creates expensive objects on demand.
3. A protection proxy controls access to the original object. Protection proxies are useful when objects should have
   different access rights.

## 7. Further reading and credits
If you wish to read more about the proxy pattern(s) here a few links I have used for this tutorial:

1. https://www.javacodegeeks.com/2015/09/proxy-design-pattern.html
2. https://www.baeldung.com/java-proxy-pattern

